@props(['id' => null, 'maxWidth' => 600])

<x-modal :id="optional($id)" :maxWith="optional($maxWith ?? 'lg')" {{ $attributes }}>
    <div class="flex items-center w-ful p-4 rounded-t-md bg-indigo-400">
        {{ $title }}
        <svg wire:click="close" class="ml-auto fill-current text-gray-700 w-6 h-6 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
            <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/>
        </svg>
    </div>
    <hr>
    <div class="px-6 py-4 mt-4">
        {{ $content }}
    </div>
    <hr>
    <div class="ml-auto p-4 rounded-B-md bg-gray-400">
        {{ $footer }}
    </div>
</x-modal>
@props([
    'type' => 'info',
    'title' => '',
    'messaje' => '',
    'color' => 'blue',
    'color' => 'icono',
])

<div {{ $atributtes }} class="w-full px-4 py-4 overflow-x-auto text-center whitespace-no-wrap rounded-md">
    @php
        switch ($type) {
            case 'success':
                $color = 'green';
                $icono = 'check';
                break;
            case 'info':
                $color = 'blue';
                $icono = 'information';
                break;
            case 'warning':
                $color = 'yellow';
                $icono = 'exclamation';
                break;
            case 'error':
                $color = 'red';
                $icono = 'x';
                break;
        }
    @endphp

    <div class="inline-flex w-full max-w-sm ml-3 overflow-hidden bg-white rounded-lg shadow-sm">
        <div class="flex items-center justify-center w-12 bg-{{ $color }}-500">
            {!! '<x-custom.toast.medium-'.$icono.'-circle' !!}
        </div>
        <div class="px-3 py-2 text-left">
            <span class="font-semibold text-{{ $color }}-500">{{ $title }}</span>
            <p class="mb-1 text-sm leading-none text-gray-500">{{ $message }}</p>
        </div>
    </div>

</div>
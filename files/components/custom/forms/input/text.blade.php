@props([
       'error' => false,
])

<input {{ $attributes }}
       type="text"
       class="form-input rounded-md shadow-sm mt-1 block w-full
       {{ $error ? ' border-red-500' : '' }}" />

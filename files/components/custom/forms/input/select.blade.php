@props([
    'options',
    'error' => false,
])

<select {{ $attributes }} class="select2 mt-1 block w-full py-2 px-3
    border bg-white rounded-md shadow-sm
    focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm
    {{ $error ? ' border-red-500' : 'border-gray-300' }}">
    <option wire:key="0" value="0">Seleccione una opción</option>
    @foreach ($options as $k => $v)
        <option wire:key="{{ $k }}" value="{{ $k }}">{{ $v }}</option>
    @endforeach
</select>
@props([
    'currency' => false,
    'calendar' => false,
    'mask' => 'Number',
    'scale' => 0,
    'thousandsSeparator' => '',
    'error' => false,
])

<div
        x-data=""
        @if ($mask == 'Number')
            x-init="IMask($refs.input, {mask: Number, min: 0, scale: '{{ $scale }}', signed: false, thousandsSeparator: '{{ $thousandsSeparator }}'});"
        @else
            x-init="IMask($refs.input, {mask: '{!! $mask !!}', min: 0, scale: '{{ $scale }}', signed: false, thousandsSeparator: '{{ $thousandsSeparator }}'});"
        @endif
        class="flex rounded-md shadow-sm mt-1
               {{ $error ? ' border-red-500' : '' }}">

    @if ($currency)
        <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
            <x-custom.icon.small-currency-dollar />
        </span>
    @endif

    <input {{ $attributes }} autocomplete="nope" type="text"
           x-ref="input"
           class="{{ $currency ? 'rounded-none rounded-r-md' : 'rounded-md'  }}
                   flex-1 form-input block w-full text-right
                   sm:text-sm sm:leading -5
                   {{ $error ? ' border-red-500' : '' }}"
    />

</div>
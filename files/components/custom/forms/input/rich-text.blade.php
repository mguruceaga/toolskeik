@props([
    'initialValue' => '',
    'error' => false,
])
<div wire:ignore
     {{ $attributes }}
     x-data
     @trix-blur="$dispatch('change', $event.target.value)">

    <input id="x" type="hidden" value="{{ $initialValue }}">
    <trix-editor input="x"
                 class="form-input rounded-md shadow-sm mt-1 block w-full
                 {{ $error ? ' border-red-500' : 'border-gray-300' }}"></trix-editor>
</div>

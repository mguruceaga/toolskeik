@props([
    'error' => false,
])

<textarea {{ $attributes }}
          type="text"
          class="form-input rounded-md shadow-sm mt-1 block w-full
          {{ $error ? ' border-red-500' : 'border-gray-300' }}">{{$slot}}</textarea>

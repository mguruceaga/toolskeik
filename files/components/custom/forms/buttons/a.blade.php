<a {{ $attributes }}
   class="inline-flex justify-center py-2 px-4 border border-transparent
   shadow-sm text-sm font-medium rounded-md text-indigo hover:bg-indigo-200
   cursor-pointer
   focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        {{ $slot }}
</a>
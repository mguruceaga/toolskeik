<?php

namespace Keikruk\Toolskeik;

use Illuminate\Support\ServiceProvider;;

class ToolskeikServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/toolskeik.php' => config_path('toolskeik.php')
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../files/components' => resource_path('views/components'),
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../files/Managers' => app_path('Managers'),
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../files/Rules' => app_path('Rules'),
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../files/Tools' => app_path('Tools'),
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../assets/css' => public_path('css'),
        ], 'toolskeik');

        $this->publishes([
            __DIR__.'/../assets/js' => public_path('js'),
        ], 'toolskeik');

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}